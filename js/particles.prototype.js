// Tool: Extend function to merge 2 objects
    var objExtend = function(obj1, obj2) {
        var extended = {};
        var prop;
        for (prop in obj1) {
            if (Object.prototype.hasOwnProperty.call(obj1, prop)) {
                extended[prop] = obj1[prop];
            }
        }
        for (prop in obj2) {
            if (Object.prototype.hasOwnProperty.call(obj2, prop)) {
                extended[prop] = obj2[prop];
            }
        }
        return extended;
    };
    var easing = {
        easeInCubic: function(x, t, b, c, d) {
            return c * (t /= d) * t * t + b;
        },
        easeOutCubic: function(x, t, b, c, d) {
            return c * ((t = t / d - 1) * t * t + 1) + b;
        },
        easeInOutCubic: function(x, t, b, c, d) {
            if ((t /= d / 2) < 1) return c / 2 * t * t * t + b;
            return c / 2 * ((t -= 2) * t * t + 2) + b;
        }
    };
// Tool: standardizing requestAnimFrame
    window.requestAnimFrame = (function() {
        return window.requestAnimationFrame ||
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame ||
            window.oRequestAnimationFrame ||
            window.msRequestAnimationFrame ||
            function(callback) {
                window.setTimeout(callback, 1000 / 60);
            };
    })();
// Tool: standardizing cancelRequestAnimFrame
    window.cancelRequestAnimFrame = (function() {
        return window.cancelAnimationFrame ||
            window.webkitCancelRequestAnimationFrame ||
            window.mozCancelRequestAnimationFrame ||
            window.oCancelRequestAnimationFrame ||
            window.msCancelRequestAnimationFrame ||
            clearTimeout;
    })();

// Tool: createjs.extend
var createjsExtend = function(subclass, superclass) {
    function o() { this.constructor = subclass; }
    o.prototype = superclass.prototype;
    return (subclass.prototype = new o());
};

// Particle System
(function(){
    this.particleJS = this.particleJS || {};

    function SetupPS(){

    }
    var p = SetupPS.prototype;
    var particleJS = function(_options){
        var _options = _options || {};
        this.startTime;
        var _defaults = {
            numParticles: 80,
            bleed: 100,
            time: 0,
            mouseX: 0,
            mouseY: 0
        };
        this.settings = objExtend(_defaults, _options);
        this.canvas = {};
        this.canvas.dom = this.settings.canvasEl || document.getElementById('canvas');
        this.canvas.ctx = this.dom.getContext("2d");
        this.canvas.width = this.dom.offsetWidth;
        this.canvas.height = this.canvas.dom.offsetHeight;
        this.animFrame = false;
    };


    particleJS.canvas.fn = {};
    particleJS.canvas.fn.prototype.events = function(){
        this.canvas.dom.addEventListener('mousemove', function(e) {
            if (this.canvas.dom == window) {
                var pos_x = e.clientX,
                    pos_y = e.clientY;
            } else {
                var pos_x = e.offsetX || e.clientX,
                    pos_y = e.offsetY || e.clientY;
            }
            this.settings.mouseX = pos_x;
            this.settings.mouseY = pos_y;
        });
        window.addEventListener('resize', function() {
            this.canvas.resize();
        });
    };
    particleJS.canvas.fn.prototype.resize = function(){
        particleJS.canvas.width = particleJS.canvas.dom.offsetWidth;
        particleJS.canvas.height = particleJS.canvas.dom.offsetHeight;
        particleJS.canvas.dom.width = particleJS.canvas.width;
        particleJS.canvas.dom.height = particleJS.canvas.height;
    };
    particleJS.canvas.fn.prototype.clear = function(){
        particleJS.canvas.ctx.clearRect(0, 0, particleJS.canvas.width, particleJS.canvas.height);
    };
    particleJS.canvas.fn.prototype.update = function(){
        particleJS.updateTime();
        particleJS.particles.update();
        particleJS.canvas.clear();
        particleJS.particles.render();
        particleJS.animFrame = requestAnimFrame(particleJS.canvas.update);
    };

// particleJS.init
    particleJS.prototype.init = function(){
        startTime = Date.now();
        particleJS.canvas.fn.events();
        particleJS.canvas.fn.resize();
        particleJS.canvas.fn.update();
    };

// particleJS.updateTime
    particleJS.prototype.updateTime = function(){
        ps.settings.time = Date.now() - startTime;
    }

// particleJS.Particles
    particleJS.prototype.particles = function(){
        // particles 
        this.nodes = [];
    };
    particleJS.particles.prototype.create = function(args){
        var p = this;
        var args = args || {};
        var _defaults = {
            position: {
                x: ps.canvas.width * Math.random(),
                y: ps.canvas.height * Math.random()
            },
            velocity: {
                current: 0,
                max: 1 * Math.random() + 1
            },
            acceleration: 0.04,
            alpha: 1,
            angle: 360 * Math.random(),
            radius: 3,
            color: "#000000"
        };
        args.options = args.options || {};
        p = objExtend(_defaults, args.options);
        return p;
    };
    particleJS.particles.prototype.update = function(args) {
        for (var i = 0; i < ps.particles.length; i++) {
            var _p = ps.particles[i];
            Object.keys(ps.fn.particles.updateFunctions).map(function(objectKey, index) {
                var _func = ps.fn.particles.updateFunctions[objectKey];
                if (_func.enabled) {
                    _func.call(_p);
                }
            });
        }
    };
    particleJS.particles.prototype.render = function(args) {
        for (var i = 0; i < ps.particles.length; i++) {
            var _p = ps.particles[i];
            Object.keys(ps.fn.particles.renderFunctions).map(function(objectKey, index) {
                var _func = ps.fn.particles.renderFunctions[objectKey];
                if (_func.enabled) {
                    _func.call(_p);
                }
            });
        }
    };
    particleJS.particles.prototype.add = function(args) {
        args.num = args.num || 1;
        for (var i = 0; i < args.num; i++) {
            var p = new ps.fn.particles.create();
            ps.particles.push(p);
        }
    };
    particleJS.particles.prototype.updateFunctions = {
        vectorMove: {
            enabled: true,
            tools: {
                slowStop: function(args){
                    for (var i = 0; i < ps.particles.length; i++) {
                        var _p = ps.particles[i];
                    }
                }
            },
            call: function(p) {
                p.position.x += p.velocity.current * Math.cos(p.angle * Math.PI / 180);
                p.position.y += p.velocity.current * Math.sin(p.angle * Math.PI / 180);
                if(p.velocity.current < p.velocity.max){
                    p.velocity.current += p.acceleration;
                }else{
                    p.velocity.current = p.velocity.max;
                }
            }
        },
        checkBounds: {
            enabled: true,
            call: function(p) {
                // reset position if off screen.
                if (p.position.x > ps.canvas.width + (p.radius * 2) + ps.settings.bleed) {
                    p.position.x = -(p.radius * 2) - ps.settings.bleed;
                } else if (p.position.x < -(p.radius * 2) - ps.settings.bleed) {
                    p.position.x = ps.canvas.width + (p.radius * 2) + ps.settings.bleed;
                }
                if (p.position.y > ps.canvas.height + (p.radius * 2) + ps.settings.bleed) {
                    p.position.y = -(p.radius * 2) - ps.settings.bleed;
                } else if (p.position.y < -(p.radius * 2) - ps.settings.bleed) {
                    p.position.y = ps.canvas.height + (p.radius * 2) + ps.settings.bleed;
                }
            }
        }
    };
    particleJS.particles.prototype.renderFunctions = {
        renderPoints: {
            enabled: true,
            call: function(p) {
                ps.canvas.ctx.save();
                ps.canvas.ctx.fillStyle = p.color;
                ps.canvas.ctx.globalAlpha = p.alpha;
                ps.canvas.ctx.beginPath();
                ps.canvas.ctx.arc(p.position.x, p.position.y, p.radius, 0, Math.PI * 2);
                ps.canvas.ctx.fill();
                ps.canvas.ctx.restore();
            }
        },
        renderLines: {
            enabled: true,
            tools: {
                lineDistance: 200,
                lineOpacity: 1,
                linkLine: function(p1, p2) {
                    var dx = p1.position.x - p2.position.x,
                        dy = p1.position.y - p2.position.y,
                        dist = Math.sqrt(dx * dx + dy * dy);

                    /* draw a line between p1 and p2 if the distance between them is under the config distance */
                    if (dist <= ps.fn.particles.renderFunctions.renderLines.tools.lineDistance) {
                        var opacity_line = ps.fn.particles.renderFunctions.renderLines.tools.lineOpacity - (dist / (1 / ps.fn.particles.renderFunctions.renderLines.tools.lineOpacity)) / ps.fn.particles.renderFunctions.renderLines.tools.lineDistance;
                        var width_line = p1.radius - (dist / (1 / p1.radius)) / ps.fn.particles.renderFunctions.renderLines.tools.lineDistance;
                        if (width_line < 0) {
                            width_line = 0;
                        }
                        if (opacity_line > 0) {
                            /* style */
                            ps.canvas.ctx.strokeStyle = 'rgba(0,0,0,' + opacity_line + ')';
                            ps.canvas.ctx.lineWidth = width_line;
                            /* path */
                            ps.canvas.ctx.beginPath();
                            ps.canvas.ctx.moveTo(p1.position.x, p1.position.y);
                            ps.canvas.ctx.lineTo(p2.position.x, p2.position.y);
                            ps.canvas.ctx.stroke();
                            ps.canvas.ctx.closePath();
                        }
                    }

                }
            },
            call: function(p) {
                for (var i = 0; i < ps.particles.length; i++) {
                    if (p !== ps.particles[i]) {
                        ps.fn.particles.renderFunctions.renderLines.tools.linkLine(p, ps.particles[i]);
                    }
                }
            }
        }
    };


}())

