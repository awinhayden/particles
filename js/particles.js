// Tool: Extend function to merge 2 objects
var objExtend = function(obj1, obj2) {
    var extended = {};
    var prop;
    for (prop in obj1) {
        if (Object.prototype.hasOwnProperty.call(obj1, prop)) {
            extended[prop] = obj1[prop];
        }
    }
    for (prop in obj2) {
        if (Object.prototype.hasOwnProperty.call(obj2, prop)) {
            extended[prop] = obj2[prop];
        }
    }
    return extended;
};
var easing = {
    easeInCubic: function(x, t, b, c, d) {
        return c * (t /= d) * t * t + b;
    },
    easeOutCubic: function(x, t, b, c, d) {
        return c * ((t = t / d - 1) * t * t + 1) + b;
    },
    easeInOutCubic: function(x, t, b, c, d) {
        if ((t /= d / 2) < 1) return c / 2 * t * t * t + b;
        return c / 2 * ((t -= 2) * t * t + 2) + b;
    }
};
// Tool: standardizing requestAnimFrame
window.requestAnimFrame = (function() {
    return window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.oRequestAnimationFrame ||
        window.msRequestAnimationFrame ||
        function(callback) {
            window.setTimeout(callback, 1000 / 60);
        };
})();
// Tool: standardizing cancelRequestAnimFrame
window.cancelRequestAnimFrame = (function() {
    return window.cancelAnimationFrame ||
        window.webkitCancelRequestAnimationFrame ||
        window.mozCancelRequestAnimationFrame ||
        window.oCancelRequestAnimationFrame ||
        window.msCancelRequestAnimationFrame ||
        clearTimeout;
})();


// Particle System
var particleJS = function(options) {
    var options = options || {};
    var ps = this;
    var startTime;
    var defaults = {
        numParticles: 80,
        bleed: 100,
        time: 0,
        mouseX: 0,
        mouseY: 0
    };
    ps.particles = [];
    ps.canvas = {
        dom: options.canvasEl || document.getElementById('canvas')
    }
    ps.canvas.ctx = ps.canvas.dom.getContext("2d");
    ps.canvas.width = ps.canvas.dom.offsetWidth;
    ps.canvas.height = ps.canvas.dom.offsetHeight;
    ps.settings = objExtend(defaults, options);
    ps.fn = {
        main: {
            init: function(args) {
                startTime = Date.now();
                ps.fn.canvas.events();
                ps.fn.canvas.resize();
                ps.fn.canvas.update();
            },
            updateTime: function() {
                ps.settings.time = Date.now() - startTime;
            }
        },
        canvas: {
            events: function(args) {
                ps.canvas.dom.addEventListener('mousemove', function(e) {
                    if (ps.canvas.dom == window) {
                        var pos_x = e.clientX,
                            pos_y = e.clientY;
                    } else {
                        var pos_x = e.offsetX || e.clientX,
                            pos_y = e.offsetY || e.clientY;
                    }
                    ps.settings.mouseX = pos_x;
                    ps.settings.mouseY = pos_y;
                    ps.fn.particles.renderFunctions.linkCursor.enabled = true;
                });
                ps.canvas.dom.addEventListener('mouseleave', function(e) {
                    ps.fn.particles.renderFunctions.linkCursor.enabled = false;
                });
                window.addEventListener('resize', function() {
                    ps.fn.canvas.resize();
                });
            },
            resize: function() {
                ps.canvas.width = ps.canvas.dom.offsetWidth;
                ps.canvas.height = ps.canvas.dom.offsetHeight;
                ps.canvas.dom.width = ps.canvas.width;
                ps.canvas.dom.height = ps.canvas.height;
            },
            clear: function() {
                ps.canvas.ctx.clearRect(0, 0, ps.canvas.width, ps.canvas.height);
            },
            update: function() {
                ps.fn.main.updateTime();
                ps.fn.particles.update();
                ps.fn.canvas.clear();
                ps.fn.particles.render();
                ps.animFrame = requestAnimFrame(ps.fn.canvas.update);
            }
        },
        particles: {
            create: function(args) {
                var args = args || {};
                var p;
                var _defaults = {
                    position: {
                        x: ps.canvas.width * Math.random(),
                        y: ps.canvas.height * Math.random()
                    },
                    velocity: {
                    	current: 0,
                    	max: 1 * Math.random() + 1
                    },
                    acceleration: 0.04,
                    alpha: 1,
                    angle: 360 * Math.random(),
                    radius: 1,
                    color: "#FFFFFF"
                };
                args.options = args.options || {};
                p = objExtend(_defaults, args.options);
                return p;
            },
            updateFunctions: {
                vectorMove: {
                    enabled: true,
                    tools: {
                    	slowStop: function(args){
                    		for (var i = 0; i < ps.particles.length; i++) {
                    			var _p = ps.particles[i];
                    		}
                    	}
                    },
                    call: function(p) {
                        p.position.x += p.velocity.current * Math.cos(p.angle * Math.PI / 180);
                        p.position.y += p.velocity.current * Math.sin(p.angle * Math.PI / 180);
                        if(p.velocity.current < p.velocity.max){
                        	p.velocity.current += p.acceleration;
                        }else{
                        	p.velocity.current = p.velocity.max;
                        }
                    }
                },
                swirls: {
                    enabled: false,
                    call: function(p) {
                        p.angle += Math.sin(ps.settings.time / 1000) * 10;
                    }
                },
                checkBounds: {
                    enabled: true,
                    call: function(p) {
                        // reset position if off screen.
                        if (p.position.x > ps.canvas.width + (p.radius * 2) + ps.settings.bleed) {
                            p.position.x = -(p.radius * 2) - ps.settings.bleed;
                        } else if (p.position.x < -(p.radius * 2) - ps.settings.bleed) {
                            p.position.x = ps.canvas.width + (p.radius * 2) + ps.settings.bleed;
                        }
                        if (p.position.y > ps.canvas.height + (p.radius * 2) + ps.settings.bleed) {
                            p.position.y = -(p.radius * 2) - ps.settings.bleed;
                        } else if (p.position.y < -(p.radius * 2) - ps.settings.bleed) {
                            p.position.y = ps.canvas.height + (p.radius * 2) + ps.settings.bleed;
                        }
                    }
                },
                slowDown: {
                    enabled: false,
                    tools: {
                        checkMoving: function() {
                            var _stillMoving = false;
                            for (var i = 0; i < ps.particles.length; i++) {
                                var _p = ps.particles[i];
                                if (_p.velocity > 0) {
                                    _stillMoving = true;
                                }
                            }
                            return _stillMoving;
                        }
                    },
                    call: function(p) {
                        if (p.velocity > 0) {
                            p.velocity -= p.acceleration;
                        } else {
                            p.velocity = 0;
                            if (!ps.fn.particles.updateFunctions.slowDown.tools.checkMoving()) {
                                ps.fn.particles.updateFunctions.vectorMove.enabled = false;
                                ps.fn.particles.updateFunctions.slowDown.enabled = false;
                                console.log('stopped');
                            }
                        }
                    }
                },
                circular: {
                    enabled: false,
                    call: function(p) {
                        var centerX = ps.canvas.width / 2;
                        var centerY = ps.canvas.height / 2;
                        var dx = p.position.x - centerX,
                                dy = p.position.y - centerY,
                                dist = Math.sqrt(dx * dx + dy * dy);
                        var _angle = Math.atan2(p.position.y - centerY, p.position.x - centerX) * 180 / Math.PI;

                        _angle += 0.15;

                        var newX = centerX + dist * Math.cos(_angle * Math.PI / 180);
                        var newY = centerY + dist * Math.sin(_angle * Math.PI / 180);
                        p.position.x = newX;
                        p.position.y = newY;
                    }
                },
            },
            renderFunctions: {
                renderPoints: {
                    enabled: true,
                    call: function(p) {
                        ps.canvas.ctx.save();
                        ps.canvas.ctx.fillStyle = p.color;
                        ps.canvas.ctx.globalAlpha = p.alpha;
                        ps.canvas.ctx.beginPath();
                        ps.canvas.ctx.arc(p.position.x, p.position.y, p.radius, 0, Math.PI * 2);
                        ps.canvas.ctx.fill();
                        ps.canvas.ctx.restore();
                    }
                },
                renderLines: {
                    enabled: true,
                    tools: {
                        lineDistance: 120,
                        lineOpacity: 1,
                        linkLine: function(p1, p2) {
                            var dx = p1.position.x - p2.position.x,
                                dy = p1.position.y - p2.position.y,
                                dist = Math.sqrt(dx * dx + dy * dy);

                            /* draw a line between p1 and p2 if the distance between them is under the config distance */
                            if (dist <= ps.fn.particles.renderFunctions.renderLines.tools.lineDistance) {
                                var opacity_line = ps.fn.particles.renderFunctions.renderLines.tools.lineOpacity - (dist / (1 / ps.fn.particles.renderFunctions.renderLines.tools.lineOpacity)) / ps.fn.particles.renderFunctions.renderLines.tools.lineDistance;
                                var width_line = p1.radius - (dist / (1 / p1.radius)) / ps.fn.particles.renderFunctions.renderLines.tools.lineDistance;
                                if (width_line < 0) {
                                    width_line = 0;
                                }
                                if (opacity_line > 0) {
                                    /* style */
                                    ps.canvas.ctx.strokeStyle = 'rgba(255,255,255,' + opacity_line + ')';
                                    ps.canvas.ctx.lineWidth = width_line;
                                    /* path */
                                    ps.canvas.ctx.beginPath();
                                    ps.canvas.ctx.moveTo(p1.position.x, p1.position.y);
                                    ps.canvas.ctx.lineTo(p2.position.x, p2.position.y);
                                    ps.canvas.ctx.stroke();
                                    ps.canvas.ctx.closePath();
                                }
                            }

                        }
                    },
                    call: function(p) {
                        for (var i = 0; i < ps.particles.length; i++) {
                            if (p !== ps.particles[i]) {
                                ps.fn.particles.renderFunctions.renderLines.tools.linkLine(p, ps.particles[i]);
                            }
                        }
                    }
                },
                linkCursor: {
                    enabled: false,
                    tools: {
                        lineDistance: 200,
                        lineOpacity: 1,
                        linkLine: function(p) {
                            var dx = p.position.x - ps.settings.mouseX,
                                dy = p.position.y - ps.settings.mouseY,
                                dist = Math.sqrt(dx * dx + dy * dy);
                            /* draw a line between p1 and p2 if the distance between them is under the config distance */
                            if (dist <= ps.fn.particles.renderFunctions.linkCursor.tools.lineDistance) {
                                var opacity_line = ps.fn.particles.renderFunctions.linkCursor.tools.lineOpacity - (dist / (1 / ps.fn.particles.renderFunctions.linkCursor.tools.lineOpacity)) / ps.fn.particles.renderFunctions.linkCursor.tools.lineDistance;
                                var width_line = p.radius - (dist / (1 / p.radius)) / ps.fn.particles.renderFunctions.linkCursor.tools.lineDistance;
                                if (width_line < 0) {
                                    width_line = 0;
                                }
                                if (opacity_line > 0) {
                                    /* style */
                                    ps.canvas.ctx.strokeStyle = 'rgba(255,255,255,' + opacity_line + ')';
                                    ps.canvas.ctx.lineWidth = width_line;
                                    /* path */
                                    ps.canvas.ctx.beginPath();
                                    ps.canvas.ctx.moveTo(p.position.x, p.position.y);
                                    ps.canvas.ctx.lineTo(ps.settings.mouseX, ps.settings.mouseY);
                                    ps.canvas.ctx.stroke();
                                    ps.canvas.ctx.closePath();

                                }
                            }

                        }
                    },
                    call: function(p) {
                        ps.fn.particles.renderFunctions.linkCursor.tools.linkLine(p);
                    }
                }
            },
            update: function(args) {
                for (var i = 0; i < ps.particles.length; i++) {
                    var _p = ps.particles[i];
                    Object.keys(ps.fn.particles.updateFunctions).map(function(objectKey, index) {
                        var _func = ps.fn.particles.updateFunctions[objectKey];
                        if (_func.enabled) {
                            _func.call(_p);
                        }
                    });
                }
            },
            render: function(args) {
                for (var i = 0; i < ps.particles.length; i++) {
                    var _p = ps.particles[i];
                    Object.keys(ps.fn.particles.renderFunctions).map(function(objectKey, index) {
                        var _func = ps.fn.particles.renderFunctions[objectKey];
                        if (_func.enabled) {
                            _func.call(_p);
                        }
                    });
                }
            },
            add: function(args) {
                args.num = args.num || 1;
                for (var i = 0; i < args.num; i++) {
                    var p = new ps.fn.particles.create();
                    ps.particles.push(p);
                }
            }
        }
    };
    ps.fn.particles.add({ num: ps.settings.numParticles });
    ps.fn.main.init();
    return ps;
};
