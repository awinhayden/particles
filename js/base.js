// So ultimately I would like to still achieve the same 
// thing as I have with regards to the multiple update and 
// render functions, just with the correct pattern.


// Particle System
var particleJS = function(options) {
    var ps = this;
    ps.particles = [];
    ps.fn = {
        canvas: {
            update: function() {
                // Main canvas loop
                //
                // Update particles: ps.fn.particles.update();
                // Render particles: ps.fn.particles.render();
                // Loop this function again (setInterval)
            }
        },
        particles: {
            create: function(args) {
                // Create and return a particle object
            },
            updateFunctions: {
                updateFunc1: {
                    // First update function/object
                },
                updateFunc2: {
                    // Second update function/object
                }
            },
            renderFunctions: {
                renderFunc1: {
                    // First render function/object
                },
                renderFunc2: {
                    // Second render function/object
                }
            },
            update: function(args) {
                for (var i = 0; i < ps.particles.length; i++) {
                    // For each particle, loop through and run each update function.
                    // Each update function in ps.fn.particles.updateFunctions is run.
                    // * Just a concern with the Javascript pattern, it does not feel efficient to use such a link reference: ps.fn.particles.updateFunctions.
                    // * Is there a better way?
                }
            },
            render: function(args) {
                for (var i = 0; i < ps.particles.length; i++) {
                    // For each particle, loop through and run each render function.
                    // Each update function in ps.fn.particles.renderFunctions is run.
                }
            }
        }
    };
    return ps;
};
// Create new particle system: var newPs = new particleJS();
