var uf = newPs.fn.particles.updateFunctions;
uf.moveToPoints = {
    enabled: false,
    points: {
        hexagon: [
            [
                756.388928416711,
                242.000000000001
            ],
            [
                625.417094957724,
                242.000000000001
            ],
            [
                532.806023374435,
                329.631886863461
            ],
            [
                532.806023374435,
                453.562089762105
            ],
            [
                625.417094957724,
                541.193976625565
            ],
            [
                756.388928416711,
                541.193976625565
            ],
            [
                849,
                453.562089762105
            ],
            [
                849,
                329.631886863461
            ]
        ],
        customShapes1: [
            [
                1072.80942935052,
                477.394005110053
            ],
            [
                1065.918345197,
                355.98538535529
            ],
            [
                1064.52852289664,
                298.317862271846
            ],
            [
                1062.48049050399,
                298.317862271846
            ],
            [
                1046.63654462046,
                359.772530284829
            ],
            [
                1012.86859559942,
                474.347117101392
            ],
            [
                974.303435714146,
                474.347117101392
            ],
            [
                941.238049801301,
                356.742477634039
            ],
            [
                927.455881494267,
                298.317862271846
            ],
            [
                925.407849101615,
                298.317862271846
            ],
            [
                924.018026801254,
                355.98538535529
            ],
            [
                917.126942647736,
                477.394005110053
            ],
            [
                881.327753030804,
                477.394005110053
            ],
            [
                895.09461742181,
                271
            ],
            [
                951.566988647832,
                271
            ],
            [
                983.256297444157,
                371.924852774292
            ],
            [
                995.6638765154,
                424.27246512386
            ],
            [
                997.053769667225,
                424.27246512386
            ],
            [
                1009.43067005494,
                373.439099684972
            ],
            [
                1041.8073088949,
                271
            ],
            [
                1096.21790254443,
                271
            ],
            [
                1110,
                477.394005110053
            ]
        ]
    },
    start: function () {
        uf.moveToPoints.events.started();
        if (uf.moveToPoints.points.customShapes1.length > newPs.particles.length) {
            newPs.particles = [];
            newPs.fn.particles.add({ num: uf.moveToPoints.points.customShapes1.length });
        }
        for (var i = 0; i < uf.moveToPoints.points.customShapes1.length; i++) {
            var _p = newPs.particles[i];
            _p.moveToPoints = true;
            _p.nextPosition = {
                x: uf.moveToPoints.points.customShapes1[i][0],
                y: uf.moveToPoints.points.customShapes1[i][1]
            };
            _p.prevPosition = {
                x: _p.position.x,
                y: _p.position.y
            };
            _p.moveTo = true;
            _p.duration = 1000 * Math.random() + 1500;
            _p.timestamp = Date.now();
        }
    },
    events: {
        started: function () {
            uf.moveToPoints.enabled = true;
            uf.vectorMove.enabled = false;
        },
        completed: function () {
            //uf.vectorMove.enabled = true;
            uf.moveToPoints.enabled = false;
        }
    },
    tools: {
        checkMove: function () {
            var _move = false;
            for (var i = 0; i < newPs.particles.length; i++) {
                var _p = newPs.particles[i];
                if (_p.moveTo) {
                    _move = true;
                }
            }
            return _move;
        }
    },
    call: function (p) {
        if(!p.moveToPoints){
            return;
        }
        var currentTime = Date.now() - p.timestamp;
        var totalTime = p.duration;


        var startValueX = p.prevPosition.x;
        var changeInValueX = p.nextPosition.x - p.prevPosition.x;

        var startValueY = p.prevPosition.y;
        var changeInValueY = p.nextPosition.y - p.prevPosition.y;

        if (currentTime < totalTime) {
            p.position.x = easing.easeInOutCubic(0, currentTime, startValueX, changeInValueX, totalTime);
            p.position.y = easing.easeInOutCubic(0, currentTime, startValueY, changeInValueY, totalTime);
        } else {
            p.moveTo = false;
        }
        if (!uf.moveToPoints.tools.checkMove()) {
            uf.moveToPoints.events.completed();
        }

    }
}